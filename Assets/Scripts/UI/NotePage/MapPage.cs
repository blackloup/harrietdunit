﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPage : MonoBehaviour {
    [SerializeField]
    GameObject doc;
    [SerializeField]
    GameObject modo;
    [SerializeField]
    GameObject btn;

    public void Click(string scene)
    {
        GameManager.Instance.SetNextScene((SceneType)Enum.Parse(typeof(SceneType), scene));
        GameManager.Instance.NextScene();
        UIManager.Instance.Note.gameObject.SetActive(false);
        UIManager.Instance.OpenNote = false;
    }

    public void OpenMap(string map)
    {
        Debug.Log(map);
        string re = map.Replace("Map_", "");
        if (re == "Map")
            btn.SetActive(true);
        if (re == "Doc")
            doc.SetActive(true);
        else if (re == "Modo")
            modo.SetActive(true);
    }
}
