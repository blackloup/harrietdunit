﻿//------------------------------------------------------------------//
// 작성자 : 조성혜
// [UI] F키 ui를 눌렀을 시 반응, 이미지 설정 
// 최종 수정일자 : 18.09.02
//------------------------------------------------------------------//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_F : MonoBehaviour {

    string objtag;
    private Text name;
    
    CameraCtrl _camera;

    Image img;
    [SerializeField]
    Sprite[] f_sprite = new Sprite[3];

    private void Awake()
    {
        name = GetComponentInChildren<Text>();
        img = GetComponent<Image>();
    }

    private void OnEnable()
    {
        _camera = GameManager.Instance.Main.GetComponent<CameraCtrl>();
    }

    void Update ()
    {
      //  if (Input.GetKeyDown(KeyCode.F))
     //       Response();
	}

    public void SetInfo(string _tag)
    {
        objtag = _tag;
        switch (objtag)
        {
            case "EXP": // 확대조사
                img.sprite = f_sprite[0];
                img.SetNativeSize();
                break;

            case "DET": // 세부조사
                img.sprite = f_sprite[1];
                img.SetNativeSize();
                break;

            default:
                img.sprite = f_sprite[2];
                img.SetNativeSize();
                break;
        }

    }

    // 반응 = 대화창
    public void Response()
    {
        //  DialoguePanel.Instance.gameObject.SetActive(true);
        GameManager.Instance.IsObserve = true;
        switch (objtag)
        {
            case "OBJ":
                UIManager.Instance.Dialoue.SetActive(true);
                DialoguePanel.Instance.StartDialogue(DialogueType.OBJ, name.text);
                break;

            case "NPC":
                UIManager.Instance.Dialoue.SetActive(true);
                DialoguePanel.Instance.StartDialogue(DialogueType.NPC, name.text);
                break;

            case "EXP": // 확대조사
                UIManager.Instance.Dialoue.SetActive(true);
                DialoguePanel.Instance.StartDialogue(DialogueType.EXP, name.text);
            //    GameManager.Instance.Event.ActiveEvent("EXP_" + name.text);
            //    gameObject.SetActive(false);
                break;

            case "DET": // 세부조사
                _camera.Details.enabled = true;
                _camera.Details.DetailsObserve(name.text);
                gameObject.SetActive(false);
                break;
        }
    }
}
