﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    // 싱글 톤
    private static UIManager _instance = null;
    public static UIManager Instance { get { return _instance; } }

    GameObject ui_tile;
    GameObject ui_game;
    GameObject ui_criminal;

    GameObject ui_panel;
    Image panel;

    bool _openNote = false;
    public bool OpenNote
    { get { return _openNote; }
        set { _openNote = value; }
    }
    //----------------- 수첩  ---------------------
    private GameObject panel_Note;
    private NotePanel _note;
    public NotePanel Note { get { return _note; } }

    // ----------------- 대화 ---------------------
    private GameObject panel_Dialogue;
    public GameObject Dialoue { get { return panel_Dialogue; } }

    //------------------- 퀘스트 ------------------------
    private QuestSystem _quest;
    public QuestSystem Quest { get { return _quest; } }
    private GameObject ui_Quest;

    // 선택지
    private SelectPanel _select;
    public SelectPanel Select { get { return _select; } }

    //-----------------미니게임--------------------------
    private RebutDialogPanel _reburt;
    public GameObject ui_reburt;

    bool _startGame = false;
    public bool StartGame { get { return _startGame; } set { _startGame = value; } }

    private void Awake()
    {
        // 씬에 이미 게임 매니저가 있을 경우
        if (_instance)
        {
            //삭제
            Destroy(gameObject);
            return;
        }

        // 유일한 게임 매니저 
        _instance = this;
        DontDestroyOnLoad(gameObject);

        // -------------- 게임 오브젝트 초기화 ----------------
    
        ui_criminal = transform.GetChild(0).gameObject;
        ui_criminal.SetActive(false);
        ui_reburt = ui_criminal.transform.GetChild(1).gameObject;
        _reburt = ui_reburt.GetComponent<RebutDialogPanel>();

        ui_game = transform.GetChild(1).gameObject;
        ui_game.SetActive(false);
        ui_tile = transform.GetChild(2).gameObject;
        ui_tile.SetActive(false);

    }

    public void Init()
    {
        ui_tile.SetActive(false);

        ui_game.SetActive(true);
        ui_criminal.SetActive(true);
        _quest = transform.GetComponentInChildren<QuestSystem>();

        // 인게임
        panel_Note = ui_game.transform.GetChild(2).gameObject;
        _note = panel_Note.GetComponent<NotePanel>();
        _note.Init();
        panel_Note.SetActive(false);

        panel_Dialogue = ui_game.transform.GetChild(3).gameObject;
        panel_Dialogue.SetActive(false);

        ui_panel = ui_game.transform.GetChild(5).gameObject;
        panel = ui_panel.GetComponent<Image>();
        ui_panel.SetActive(false);

        ui_Quest = _quest.gameObject;
         _quest.InitQuest();

        _select = GetComponentInChildren<SelectPanel>();

    }

    public void PlayMiniGame()
    {
        ui_reburt.SetActive(true);
        _reburt.StartRebut(CriminalType.Mini);
    }

    public void OnTitle()
    {
        ui_tile.SetActive(true);
    }
    public void OffTitle()
    {
        ui_tile.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKey(KeyCode.Tab))
        {
            panel_Note.SetActive(true);
            _openNote = true;
        }
    }

    //============================================
    //         씬 변경시 유아이 설정
    //============================================
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }


    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
       SceneType cur = GameManager.Instance.CurScene;

        if (cur == SceneType.Title)
        {
            ui_tile.SetActive(true);
            ui_game.SetActive(false);
        }
        else
        {
        
            ui_tile.SetActive(false);

            ui_game.SetActive(true);

            GameManager.Instance.MainVirtual = GameObject.Find("V_Start");
            //if (GameManager.Instance.IsLoad)
            //{
                _quest.InitQuest();
            //    _note.Init();
            //}
            //else
            //    GameManager.Instance.IsLoad = false;

            
        }
    }

    //============================================
    //        Fade In / Out
    //============================================

    public void StartFadeIn()
    {
        ui_panel.SetActive(true);
        StartCoroutine(FadeIn());
    }
    public void StartFadeOut()
    {
        ui_panel.SetActive(true);
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeIn()
    {
        GameManager.Instance.IsObserve = true;
        Color color = Color.black;
        while (color.a > 0)
        {
            color.a -= Time.deltaTime;
            panel.color = color;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        ui_panel.SetActive(false);
        GameManager.Instance.IsObserve = false;
        yield break;
    }

    IEnumerator FadeOut()
    {
        GameManager.Instance.IsObserve = true;
        Color color = panel.color;
        while (color.a < 1)
        {
            color.a += Time.deltaTime;
            panel.color = color;
            yield return new WaitForSeconds(Time.deltaTime);
        }

        EventCamera e = GameObject.FindObjectOfType<EventCamera>();
        e.transform.gameObject.SetActive(false);

        GameObject _main = GameManager.Instance.Main;
        _main.SetActive(true);
        GameManager.Instance.PlayerEnable(true);
        while (color.a > 0)
        {
            color.a -= Time.deltaTime;
            panel.color = color;

            yield return new WaitForSeconds(Time.deltaTime);
        }

        ui_panel.SetActive(false);

        yield return new WaitForSeconds(3.0f);

        GameManager.Instance.IsObserve = false;
        GameManager.Instance.bPlayEvent = false;

        if (_startGame)
            PlayMiniGame();

        if (GameManager.Instance.IsNext) GameManager.Instance.NextScene();

        yield break;
    }

}
