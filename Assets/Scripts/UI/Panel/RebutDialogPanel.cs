﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;
public class RebutDialogPanel : UIPanel {

    GameObject[] _life;
    CriminalType _type;
    // ------------- xml -------------------
    XmlDocument doc;
    XmlNode curNode;
    TextAsset Ctxt;

    XmlDocument gDoc;
    TextAsset gTxt;
    //--------------- 대화 --------------------
    string content;          // 대화 내용 

    private bool isTurn = false;    //다음 대화
    string nextNum = null;               //다음 대화 id
    private int curPos = 0;         // 글자 순서 

    //텍스트
    private Text txt_name;          // 캐릭터 이름 
    private Text txt_chat;          // 대화 내용 

    // 이미지
    private RectTransform bg_rect;
    private Image bg_img;

    GameObject[] sel;

    void Awake ()
    {
        Ctxt = (TextAsset)Resources.Load("Xml/Criminal");
        gTxt = (TextAsset)Resources.Load("Xml/CrimGroup");

        bg_rect = transform.GetChild(0).GetComponent<RectTransform>();
        bg_img = transform.GetChild(0).GetComponent<Image>();
        txt_name = transform.GetChild(2).GetComponent<Text>();
        txt_chat = transform.GetChild(3).GetComponent<Text>();

        _life = new GameObject[5];
        for(int i=0; i<5; i++)
        {
            _life[i]=transform.GetChild(4).GetChild(i).gameObject;
        }

        sel = new GameObject[3];
        for(int i = 0; i < 3; i++)
        {
            sel[i] = transform.GetChild(5).GetChild(i).gameObject;
            sel[i].SetActive(false);
        } 
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            ClickDialogue();
    }
    public void StartRebut(CriminalType type)
    {

        _type = type;
        string id = "";
        switch (_type)
        {
            case CriminalType.Willie:
                id = "Sel_20001";
                break;
            case CriminalType.Modo:
                id = "Sel_30001";
                break;
            case CriminalType.Victoria:
                id = "Sel_40001";
                break;
            case CriminalType.Doctor:
                id = "Sel_50001";
                break;
            default:
                id = "Sel_10001";
                break;
        }
        doc = new XmlDocument();
        doc.LoadXml(Ctxt.text);
        XmlNode criminal = doc.SelectSingleNode("Criminal/" + _type.ToString());
        curNode = criminal.SelectSingleNode("CrimSel[@ID='" + id + "']");

        txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
        content = curNode.SelectSingleNode("ScriptChText").InnerText;

        string str_next = curNode.SelectSingleNode("ScriptNextID").InnerText;

        // 다음 스크립트가 있을때
        if (str_next != null)
        {
            nextNum = str_next;
        }
        else nextNum = null;

        StartCoroutine(ShowingDialogue());
        CheckOption();
    }

    public void StartRebut(string str_id)
    {
        doc = new XmlDocument();
        doc.LoadXml(Ctxt.text);

        XmlNode criminal = doc.SelectSingleNode("Criminal/" + _type.ToString());
        curNode = criminal.SelectSingleNode("CrimSel[@ID='" + str_id + "']");

        txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
        content = curNode.SelectSingleNode("ScriptChText").InnerText;

        string str_next = curNode.SelectSingleNode("ScriptNextID").InnerText;

        // 다음 스크립트가 있을때
        if (str_next != null)
        {
            nextNum = str_next;
        }
        else nextNum = null;

        StartCoroutine(ShowingDialogue());
        CheckOption();
    }

    List<string> nextList = new List<string>();
    void SetSelect(string groupID)
    {
        gDoc = new XmlDocument();
        gDoc.LoadXml(gTxt.text);
        nextList.Clear();

        XmlNodeList list = gDoc.SelectNodes("CrimGroup/Think[@GroupID='" + groupID + "']");
        for (int i = 0; i < list.Count; i++)
        {
            string next = list[i].SelectSingleNode("ScriptNextID").InnerText;
            nextList.Add(next);

            //버튼
            sel[i].GetComponent<Text>().text = list[i].SelectSingleNode("ScriptDesc").InnerText;
            sel[i].SetActive(true);
        }

    }
    public void ClickSelect(int num)
    {

        //선택지 끄기 
        for (int i = 0; i < 3; i++)
            sel[i].SetActive(false);

        if (num >= 0)
        {
            Debug.Log(num + " ? " + nextList[num]);
            StartRebut(nextList[num]);
           
        }
    }

    void CheckOption()
    {
        XmlNode option = curNode.SelectSingleNode("Option");
        if (option != null)
        {
            //일러스트
            if (option.Attributes["BGNameTrans"] != null)
            {
                string bgName = option.Attributes["BGNameTrans"].Value;

                // n일경우 종료
                if (bgName == "n")
                    bg_rect.gameObject.SetActive(false);
                //아닌 경우 새로 띄우거나 그림 변경
                else
                {
                    if (!bg_rect.gameObject.activeSelf) bg_rect.gameObject.SetActive(true);

                    bg_img.sprite = Resources.Load<Sprite>("Dial/" + bgName);
                    // 이미지 설정
                    bg_img.SetNativeSize();
                }
            }
            //선택지 
            if (curNode.SelectSingleNode("GroupID") != null)
            {
                SetSelect(curNode.SelectSingleNode("GroupID").InnerText);
            }
            //애니메이션 제어
            //카메라 제어 
        }
    }
    void NextScript(string nextID)
    {
        curNode = doc.SelectSingleNode("Criminal/" + _type + "/CrimSel[@ID='" + nextID + "']");

        txt_name.text = curNode.SelectSingleNode("ScriptChName").InnerText;
        content = curNode.SelectSingleNode("ScriptChText").InnerText;

        isTurn = false;

        string str_nextId = curNode.SelectSingleNode("ScriptNextID").InnerText;

        //next id 를 판단해 다음 대사 출력
        if (str_nextId != null)
        {
            nextNum = str_nextId;
        }

        StartCoroutine(ShowingDialogue());
        CheckOption();
    }

    public void ClickDialogue()
    {
        if (isTurn)
        {
            ClickSelect(-1);
            if (nextNum != null)
            {
                NextScript(nextNum);
            }
            else
            {
                XmlNode option = curNode.SelectSingleNode("Option");
                if (option.Attributes["DownLife"] != null)
                {
                    int count = _life.Length-1;
                    if (count <= 0)
                        Debug.Log(name +":: Game Over");
                    else
                    {
                        _life[count].SetActive(false);
                    }
                }

                if (option.Attributes["MonoScriptID"] != null)
                {
                    int id = int.Parse(option.Attributes["MonoScriptID"].Value);
                    UIManager.Instance.Dialoue.SetActive(true);
                    DialoguePanel.Instance.Monologue(id);
                }

                GameManager.Instance.IsObserve = false;
                gameObject.SetActive(false);
            }
        }
        else
        {
            isTurn = true;
        }

    }

    IEnumerator ShowingDialogue()
    {
        int num = content.Length;

        while (curPos < num && !isTurn)
        {
            txt_chat.text = content.Substring(0, curPos);
            ++curPos;

            yield return new WaitForSeconds(0.05f);
        }

        curPos = 0;
        txt_chat.text = content;
        isTurn = true;
        yield break;
    }
}
