﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class NotePanel : UIPanel
{
    NotePage note;
    InfPage inf;

    ScrollRect left;
    Scrollbar bar;

    RectTransform char_L;
    RectTransform clue_L;
    GameObject char_R;
    GameObject clue_R;

    GameObject map;
    MapPage mapP;

    int open = -1;

	void Awake ()
    {
        note = transform.GetChild(1).GetComponent<NotePage>();
        bar = transform.GetChild(1).GetChild(0).GetComponentInChildren<Scrollbar>();

        left = transform.GetChild(1).GetChild(0).GetComponent<ScrollRect>();
        char_L = transform.GetChild(1).GetChild(0).GetChild(1).GetComponent<RectTransform>();
        clue_L = transform.GetChild(1).GetChild(0).GetChild(2).GetComponent<RectTransform>();

        char_R = transform.GetChild(1).GetChild(1).GetChild(0).gameObject;
        clue_R = transform.GetChild(1).GetChild(1).GetChild(1).gameObject;

        //  inf = transform.GetChild(1).GetChild(4).GetComponent<InfPage>();

        map = transform.GetChild(3).gameObject;
        mapP = map.GetComponent<MapPage>();
    }

    public void Init()
    {
        note.Init();
        // 추론 
    }

    public void Click(int num)
    {
        if (open != num)
        {
            if(!note.gameObject.activeSelf)
                note.gameObject.SetActive(true);

            char_R.SetActive(false);
            clue_R.SetActive(false);

            open = num;
            switch (num)
            {
                case -1:
                    UIManager.Instance.OpenNote = true;
                    left.content = char_L;
                    char_L.gameObject.SetActive(true);
                    clue_L.gameObject.SetActive(false);
                    clue_R.gameObject.SetActive(false);
                    map.SetActive(false);
                    // inf.gameObject.SetActive(false);
                    break;
                case 0:
                    left.content = char_L;
                    char_L.gameObject.SetActive(true);
                    clue_L.gameObject.SetActive(false);
                    clue_R.gameObject.SetActive(false);
                    map.SetActive(false);
                    // inf.gameObject.SetActive(false);
                    break;

                case 1:
                    left.content = clue_L;
                    char_L.gameObject.SetActive(false);
                    clue_L.gameObject.SetActive(true);
                    char_R.gameObject.SetActive(false);
                    map.SetActive(false);
                    //  inf.gameObject.SetActive(false);
                    break;

                case 2:
                    inf.gameObject.SetActive(true);
                    break;

                case 3:
                    map.SetActive(true);
                    note.gameObject.SetActive(false);
                    break;

                default:
                    break;
            }
        }
    }

    public void MapOpen(string str)
    {
        mapP.OpenMap(str);
    }

    //초기화

    // 단서 추가 
    public void Add(string checkID)
    {
        XmlDocument doc = new XmlDocument();
        //추론 메인
        if (checkID.Contains("L"))
        {
            doc.Load(Application.dataPath + "Play_InfM.xml");
            XmlNode node = doc.SelectSingleNode("Main/field[@SubjectListID='" + checkID + "']");
            node.Attributes["DefaultState"].Value = "on";
            doc.Save(Application.dataPath + "Play_InfM.xml");

            inf.Add(checkID);

            Debug.Log(name + ": Add 추론메인 :" + checkID);
            UIManager.Instance.Quest.QuestCheck(checkID);
        }
    }
    public void Add(int checkID)
    {
        XmlDocument doc = new XmlDocument();

        // 세부 추론
        if (checkID > 50000)
        {
            doc.Load(Application.dataPath + "Play_InfS.xml");
            XmlNodeList groupList = doc.SelectSingleNode("Sub").ChildNodes;
            foreach (XmlNode group in groupList)
            {
                XmlNode node = group.SelectSingleNode("SubInfo[@QuestionID='" + checkID + "']");
                if (node != null)
                {
                    node.Attributes["DefaultState"].Value = "on";
                    doc.Save(Application.dataPath + "/Play_InfS.xml");
                    Debug.Log(name + ": Add 세부추론 :" + checkID);
                    break;
                }
            }
        }
        // 증거 증언
        else
        {
            doc.Load(Application.dataPath + "/Play_note.xml");
            XmlNodeList typeList = doc.SelectSingleNode("Note").ChildNodes;
            foreach(XmlNode type in typeList)
            {
                XmlNode node = type.SelectSingleNode("Clue[@ID='" + checkID + "']");
                if(node != null)
                {
                    node.Attributes["DefaultState"].Value = "on";
                    doc.Save(Application.dataPath + "/Play_note.xml");

                    string name = node.SelectSingleNode("EvidenceNameKr").InnerText;

                    if (name == "n")
                    {
                        note.Add(checkID);
                    }
                    else
                    {
                        string _prefabName = null;
                        if (node.SelectSingleNode("EvidenceDefaultName") != null)
                        {
                            _prefabName = node.SelectSingleNode("EvidenceDefaultName").InnerText;
                        }
                        string desc = null;
                        if (node.SelectSingleNode("EvidenceDesc") != null)
                            desc = node.SelectSingleNode("EvidenceDesc").InnerText;

                        Slot _slot = new Slot(checkID, name, _prefabName, desc);
                        if (type.Name == "Char")
                            note.AddChar(_slot);
                        else
                            note.AddClue(_slot);
                    }
                    XmlNode option = node.SelectSingleNode("Option");
                    if(option != null)
                    {
                        CheckOption(option);
                    }

                    Debug.Log(name + ": Add 증거 증언 :" + checkID);
                    break;
                }
            }

        }
        UIManager.Instance.Quest.QuestCheck(checkID.ToString());
    }

	public void CheckOption(XmlNode option)
    {

        if (option.Attributes["GetOffStateID"] != null)
        {
            string onStateID = option.Attributes["GetOffStateID"].Value;

            string[] IDs = onStateID.Split('/');

            for (int i = 0; i < IDs.Length; i++)
            { 
                int _id = int.Parse(IDs[i]);

                //탐색
                if (_id >= 1000 && _id < 2000)
                {
                    DialoguePanel.Instance.SetObserveState(_id, false);
                }
                //선택지
                else if (_id >= 7000 && _id < 8000)
                {

                }
                //단서
                else
                {

                }
            }
        }

        if (option.Attributes["GetOnStateID"] != null)
        {
            string onStateID = option.Attributes["GetOnStateID"].Value;

            string[] IDs = onStateID.Split('/');

            for (int i = 0; i < IDs.Length; i++)
            {
                int _id;
                bool val = int.TryParse(IDs[i], out _id);

                if (val)
                {
                    //탐색
                    if (_id >= 1000 && _id < 2000)
                    {
                        DialoguePanel.Instance.SetObserveState(_id, true);
                    }
                    //선택지
                    else if (_id >= 7000 && _id < 8000)
                    {

                    }
                    //단서 
                    else
                    {
                        Add(_id);
                    }
                }
                else
                {
                     //퀘스트 
                     if (IDs[i].Contains("Q"))
                    {
                        UIManager.Instance.Quest.AddQuest(IDs[i]);
                    }
                    // 추론 메인
                    Add(IDs[i]);
                }
            }
        }
        
    }
}