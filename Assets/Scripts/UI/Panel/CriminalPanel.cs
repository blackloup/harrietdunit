﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CriminalType
{
    Mini,
    Willie,
    Modo,
    Victoria,
    Doctor
}

public class CriminalPanel : UIPanel {

    CriminalType _cType;

    [SerializeField]
    RebutDialogPanel _rebutDial;

    public void SelectCriminal(CriminalType type)
    {
        _cType = type;

        _rebutDial.gameObject.SetActive(true);
        _rebutDial.StartRebut(type);

        PopDown();
    }
	
}
