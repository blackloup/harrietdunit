﻿//------------------------------------------------------------------//
// 작성자 : 조성혜
// 퀘스트를 관리 하는 코드
// 최근 수정 사항: 저장데이터를 불러올때 제대로 퀘스트 체크가 되지 않던걸 해결
// 최종 수정일자 : 18.06.06
//------------------------------------------------------------------//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.UI;

struct QuestSub
{
    public int id;
    public string QuestScript;
    public List<string> provisoID;

    public int checkCount;//퀘스트 체크갯수
    public int uiIndex; // ui 순서
}

struct QuestTitle
{
    public int id;
    public bool DefaultSate;
    public string questUIText;
    public int MonologueNextID;
    public List<string> GetOffStateID;
    public List<string> GetOnStateID;
    public string NextSceneName;

    public int uiIndex;

    public List<QuestSub> quest;

}

public class QuestSystem : MonoBehaviour {

    XmlDocument QLDoc;
    XmlDocument QTDoc;

    TextAsset QTText;

    List<QuestTitle> questList;
    List<GameObject> uiList;

    RectTransform parent;
    public GameObject prefab_title;
    public GameObject prefab_script;

    bool _isEndQuest = false;
    public bool IsEndQuest {
        get { return _isEndQuest; }
        set { _isEndQuest = false; }
    }

    int _end;
    string endChkID = "";
    public string EndChkID
    {
        get { return endChkID; }
    }


    string curScene;

    void Awake() {
        questList = new List<QuestTitle>();
        uiList = new List<GameObject>();

        QTText = (TextAsset)Resources.Load("Xml/QT");

        parent = transform.GetChild(1).GetComponent<RectTransform>();
    }

    public void AddQuest(string Questid, bool endchk = false)
    {
#if UNITY_EDITOR
        if (Questid == null)
            Debug.Log(name + ": Quest Error");
#endif

        string str = Questid.Replace("QL_", "");
        int qID = int.Parse(str);
        if (endchk) _end = qID;

        bool _have = false;
        foreach (QuestTitle t in questList)
        {
            if (t.id == qID)
            {
                _have = true;
                break;
            }
        }

        if (!_have)
        {
            QLDoc.Load(Application.dataPath + "/Play_QL.xml");

            curScene = GameManager.Instance.CurScene.ToString();
            XmlNode QLNode = QLDoc.SelectSingleNode("QL/" + curScene + "/Quest[@ID='" + Questid + "']");

            if (QLNode == null)
                FixXml(qID, "on");
            else
            {
                string state = QLNode.Attributes["DefaultState"].Value;
                if (state == "off")
                {
                    QuestTitle title = new QuestTitle();

                    title.id = qID;
                    title.DefaultSate = true;
                    title.uiIndex = uiList.Count;

                    string uiText = QLNode.SelectSingleNode("QuestUIText").InnerText;
                    title.questUIText = uiText;

                    XmlNode option = QLNode.SelectSingleNode("Option");

                    int monologueID = 0;
                    if (QLNode.SelectSingleNode("MonologueID") != null)
                    {
                        string str_mono = QLNode.SelectSingleNode("MonologueID").InnerText;
                        monologueID = int.Parse(str_mono);
                    }
                    title.MonologueNextID = monologueID;

                    title.GetOffStateID = new List<string>();
                    if (option.Attributes["GetOffStateID"] != null)
                    {
                        string str_off = option.Attributes["GetOffStateID"].Value;
                        string[] IDs = str_off.Split('/');

                        foreach (string offID in IDs)
                            title.GetOffStateID.Add(offID);
                    }

                    title.GetOnStateID = new List<string>();
                    if (option.Attributes["GetOnStateID"] != null)
                    {
                        string str_on = option.Attributes["GetOnStateID"].Value;
                        string[] IDs = str_on.Split('/');

                        foreach (string onID in IDs)
                            title.GetOnStateID.Add(onID);
                    }

                    string nextScene = null;
                    if (QLNode.SelectSingleNode("NextSceneName") != null)
                    {
                        nextScene = QLNode.SelectSingleNode("NextSceneName").InnerText;
                    }
                    title.NextSceneName = nextScene;

                    // 퀘스트 타이틀 그룹의 퀘스트테이블 리스트 제작
                    List<QuestSub> subList = new List<QuestSub>();

                    QTDoc = new XmlDocument();
                    QTDoc.LoadXml(QTText.text);
                    XmlNodeList nodelist = QTDoc.SelectNodes("QT/" + curScene + "/Title[@QuestGroupID='" + Questid + "']");

                    int uiIndex = 1;
                    foreach (XmlNode node in nodelist)
                    {
                        QuestSub sub = new QuestSub();

                        string str_qID = node.SelectSingleNode("Sub").Attributes["ID"].Value;
                        string reID = str_qID.Replace("Q_", "");
                        sub.id = int.Parse(reID);

                        string script = node.SelectSingleNode("Sub").Attributes["QuestScript"].Value;
                        sub.QuestScript = script;

                        sub.provisoID = new List<string>();

                        if (node.SelectSingleNode("Sub").Attributes["GetProvisoID"] != null)
                        {
                            string str_proviso = node.SelectSingleNode("Sub").Attributes["GetProvisoID"].Value;
                            string[] IDs = str_proviso.Split('/');

                            foreach (string inID in IDs)
                                sub.provisoID.Add(inID);
                        }

                        sub.checkCount = sub.provisoID.Count;

                        sub.uiIndex = uiIndex;
                        uiIndex++;

                        subList.Add(sub);
                    }
                    title.quest = subList;
                    questList.Add(title);

                    FixXml(qID, "ing");
                    CreateUI(questList.Count - 1);
                }
            }

        }
    }

    public void InitQuest()
    {

        //if (questList != null)
        //{
        //    questList.Clear();
        //    foreach (GameObject ui in uiList)
        //    {
        //        ui.transform.SetParent(null);
        //        Destroy(ui);
        //    }
        //    uiList.Clear();
        //}

        QLDoc = new XmlDocument();
        QLDoc.Load(Application.dataPath + "/Play_QL.xml");

        curScene = GameManager.Instance.CurScene.ToString();
        XmlNodeList sceneList = QLDoc.SelectNodes("QL/" + curScene+ "/Quest[@DefaultState='on']");
        //현재 씬에 on 되어있는 퀘스트타이틀 들을 리스트에 추가.
        foreach (XmlNode QLNode in sceneList)
        {
            //완료된 퀘스트가 아닌경우 

            QuestTitle title = new QuestTitle();

            string str_id = QLNode.Attributes.GetNamedItem("ID").Value;
            string reId = str_id.Replace("QL_", "");    // 아이디의 영문 제거 
            int id = int.Parse(reId);
            title.id = id;

            title.DefaultSate = true;
            title.uiIndex = uiList.Count;

            string uiText = QLNode.SelectSingleNode("QuestUIText").InnerText;
            title.questUIText = uiText;

            XmlNode option = QLNode.SelectSingleNode("Option");

            int monologueID = 0;
            if (QLNode.SelectSingleNode("MonologueID") != null)
            {
                string str_mono = QLNode.SelectSingleNode("MonologueID").InnerText;
                monologueID = int.Parse(str_mono);
            }
            title.MonologueNextID = monologueID;

            title.GetOffStateID = new List<string>();
            if (option.Attributes["GetOffStateID"] != null)
            {
                string str_off = option.Attributes["GetOffStateID"].Value;
                string[] IDs = str_off.Split('/');

                foreach (string offID in IDs)
                    title.GetOffStateID.Add(offID);
            }

            title.GetOnStateID = new List<string>();
            if (option.Attributes["GetOnStateID"] != null)
            {
                string str_on = option.Attributes["GetOnStateID"].Value;
                string[] IDs = str_on.Split('/');

                foreach (string onID in IDs)
                    title.GetOnStateID.Add(onID);
            }

            string nextScene = null;
            if (QLNode.SelectSingleNode("NextSceneName") != null)
            {
                nextScene = QLNode.SelectSingleNode("NextSceneName").InnerText;
            }
            title.NextSceneName = nextScene;

            // 퀘스트 타이틀 그룹의 퀘스트테이블 리스트 제작
            List<QuestSub> subList = new List<QuestSub>();

            QTDoc = new XmlDocument();
            QTDoc.LoadXml(QTText.text);
            XmlNodeList nodelist = QTDoc.SelectNodes("QT/" + curScene + "/Title[@QuestGroupID='" + str_id + "']");

            int uiIndex = 1;
            foreach (XmlNode node in nodelist)
            {
                QuestSub sub = new QuestSub();

                string str_qID = node.SelectSingleNode("Sub").Attributes["ID"].Value;
                string reID = str_qID.Replace("Q_", "");
                sub.id = int.Parse(reID);

                string script = node.SelectSingleNode("Sub").Attributes["QuestScript"].Value;
                sub.QuestScript = script;

                sub.provisoID = new List<string>();

                if (node.SelectSingleNode("Sub").Attributes["GetProvisoID"] != null)
                {
                    string str_proviso = node.SelectSingleNode("Sub").Attributes["GetProvisoID"].Value;
                    string[] IDs = str_proviso.Split('/');

                    foreach (string inID in IDs)
                        sub.provisoID.Add(inID);
                }

                sub.checkCount = sub.provisoID.Count;

                sub.uiIndex = uiIndex;
                uiIndex++;

                subList.Add(sub);
            }
            title.quest = subList;
            questList.Add(title);

            FixXml(title.id, "ing");
            CreateUI(questList.Count - 1);

        }
    }

    void CheckTitle()
    {
        for (int i = questList.Count - 1; i >= 0; i--)
        {
            //모든 세부 퀘스트가 끝났을 때 = 메인 퀘스트 완료 
            if (questList[i].quest.Count == 0 && questList[i].DefaultSate)
            {
                _isEndQuest = true;
                if (questList[i].GetOnStateID != null)
                {
                    // on
                    foreach (string onID in questList[i].GetOnStateID)
                    {
                        //추론 단서일때 
                        if (onID.Contains("CH"))
                            UIManager.Instance.Note.Add(onID);
                        else if (onID.Contains("QL_"))
                            AddQuest(onID);
                        else
                        {
                            int _id = int.Parse(onID);

                            // 오브젝트 조사 
                            if (_id >= 10000 && _id < 20000)
                            {
                                DialoguePanel.Instance.SetObserveState(_id, true);
                            }
                            //단서(증거 증언 세부추론)
                            if ((_id >= 30000 && _id < 40000) || _id >= 50000)
                                UIManager.Instance.Note.Add(_id);

                            // 선택지
                            UIManager.Instance.Select.SetSelectState(_id, true);
                        }
                    }
                }

                // 독백
                if (questList[i].MonologueNextID > 0)
                {
                    UIManager.Instance.Dialoue.SetActive(true);
                    DialoguePanel.Instance.Monologue(questList[i].MonologueNextID);
                }

                if(questList[i].NextSceneName != null)
                {
                    Debug.Log("--------------------"+questList[i].NextSceneName);
                    if(questList[i].NextSceneName.Contains("G"))
                    {
                        Debug.Log("GGGGGGAAAAAMMMMEEEE");
                        UIManager.Instance.StartGame = true;
                        if (!DialoguePanel.Instance.IsTalk && !GameManager.Instance.bPlayEvent)
                            UIManager.Instance.PlayMiniGame();
                    }
                }

                // xml 상태 변경
                FixXml(questList[i].id, "complete");

                int index = questList[i].uiIndex;

                // 타 메인 퀘스트 ui  위치 수정 
                for (int num = index + 1; num < uiList.Count; num++)
                {
                    GameObject ui = uiList[num];
                    RectTransform rect = ui.GetComponent<RectTransform>();
                    rect.anchoredPosition = uiList[num - 1].GetComponent<RectTransform>().anchoredPosition;
                }

                // ui 삭제
                Destroy(uiList[index]);
                uiList.RemoveAt(index);

                QuestTitle node = questList[i];
                node.DefaultSate = false;
                node.uiIndex = -1;
                questList[i] = node;

                ChangeIndex(index);
            }
        }
        //qusetList count가 0인경우 리스트 초기화
        if (uiList.Count <= 0)
        {
            questList.Clear();
        }
    }

    void ChangeIndex(int index)
    {
      
        for(int i = questList.Count -1; i> index; i--)
        {

                QuestTitle node = questList[i];
                node.uiIndex = questList[i].uiIndex - 1; ;
                questList[i] = node;
        }
    }

    // 단서 수집 시 완료되는 퀘스트가 있는 지 확인
    public void QuestCheck(string checkID)
    {
        bool isEnd = false;
        foreach (QuestTitle title in questList)
        {
            if (title.DefaultSate)
            {
                foreach (QuestSub sub in title.quest)
                {
                    if (sub.provisoID.Contains(checkID))
                    {
                        //세부 퀘스트 단서 목록에서 지우기
                        sub.provisoID.Remove(checkID);

                        // 세부 퀘스트 UI
                        GameObject subUI = uiList[title.uiIndex].transform.GetChild(sub.uiIndex).gameObject;

                        // 세부퀘스트 수 갱신
                        int count = sub.checkCount - sub.provisoID.Count;
                        Text countTxt = subUI.transform.GetChild(0).GetComponent<Text>();
                        countTxt.text = count.ToString();

                        // 확대 조사일 경우 세부 퀘스트 내용 수정
                        Text scriptTxt = subUI.GetComponent<Text>();
                        if (title.id > 9500 & scriptTxt.text == "???")
                            scriptTxt.text = sub.QuestScript;

                        // 세부 퀘스트 완료시 ui 변경
                        if (sub.provisoID.Count <= 0)
                        {
                            isEnd = true;
                            // 세부 퀘스트 삭제
                            title.quest.Remove(sub);
                            //회색 처리
                            scriptTxt.color = Color.gray;
                            countTxt.color = Color.gray;
                            subUI.transform.GetChild(1).GetComponent<Text>().color = Color.gray;
                            subUI.transform.GetChild(2).gameObject.SetActive(true);
                        }
                        break;
                    }
                }
            }
        }
        if (isEnd)
        {
            isEnd = false;
            CheckTitle();
        }
    }

    // 데이터 로드 시 진행 중이던 세부 퀘스트확인
    public void CheckedQuest()
    {
        foreach(QuestTitle title in questList)
        {
            // on 상태인 메인 퀘스트의 세부 퀘스트들
            if(title.DefaultSate)
            {
                foreach(QuestSub sub in title.quest)
                {
                    foreach (string proviso in sub.provisoID)
                    {
                        bool bcheck = false;

                        int num;
                        if (int.TryParse(proviso, out num))
                        {
                            //오브젝트 조사 
                            //인물,증거
                            Debug.Log(name + "CheckedQuset : 오브젝트 조사/ 인물,증거");
                        }
                        else
                        {
                            // 추론
                            Debug.Log(name + "CheckedQuset :추론");
                        }

                        // 완료된 퀘스트인 경우
                        if (bcheck)
                        {
                            // 단서 목록에서 지우기
                            sub.provisoID.Remove(proviso);

                            // ui 갱신 
                            GameObject subUI = uiList[title.uiIndex].transform.GetChild(sub.uiIndex).gameObject;

                            int count = sub.checkCount - sub.provisoID.Count;
                            Text countTxt = subUI.transform.GetChild(0).GetComponent<Text>();
                            countTxt.text = count.ToString();

                            // 확대 조사퀘스트일 경우 세부 퀘스트 내용 수정
                            Text scriptTxt = subUI.GetComponent<Text>();
                            if (title.id > 9500 & scriptTxt.text == "???")
                                scriptTxt.text = sub.QuestScript;

                            // 세부 퀘스트 완료시 ui 변경
                            if (sub.provisoID.Count <= 0)
                            {
                                // 세부 퀘스트 삭제
                                title.quest.Remove(sub);
                                //회색 처리
                                scriptTxt.color = Color.gray;
                                countTxt.color = Color.gray;
                                subUI.transform.GetChild(1).GetComponent<Text>().color = Color.gray;
                                subUI.transform.GetChild(2).gameObject.SetActive(true);
                            }

                        }
                    }
                }
            }
        }
    }

    public bool CheckEndEvent(string id)
    {
        if (questList == null) return true;
        foreach(QuestTitle title in questList)
        {
            if("QL_" + title.id == id)
            {
                if(title.quest.Count == 0)
                    return true;
            }
        }

        return false;
    }

    void FixXml(int titleID,string state)
    {
        QLDoc = new XmlDocument();
        QLDoc.Load(Application.dataPath + "/Play_QL.xml");

        curScene = GameManager.Instance.CurScene.ToString();
        XmlNode node = QLDoc.SelectSingleNode("QL/" + curScene + "/Quest[@ID='QL_"+titleID+"']");

        if(node == null)
        {
            XmlNodeList list = QLDoc.SelectSingleNode("QL").ChildNodes;
            foreach(XmlNode n in list)
            {
               node= n.SelectSingleNode("Quest[@ID='QL_" + titleID + "']");
                if (node != null) break;

            }
        }
        node.Attributes["DefaultState"].Value = state;

        QLDoc.Save(Application.dataPath + "/Play_QL.xml");
    }

    void CreateUI(int index)
    {
        //현재 on 상태인 퀘스트라면
        if ( questList.Count > uiList.Count)
        {
            // 게임 오브젝트 생성
            GameObject newQuest = Instantiate(prefab_title);

            // 서브 퀘스트 갯수 , 현재 생성된 퀘스트 ui 갯수 
            int questCount = questList[index].quest.Count;
            int uiCount = parent.childCount;


            // text 지정 및 부모 지정
            newQuest.transform.GetChild(0).GetComponent<Text>().text = questList[index].questUIText;
            newQuest.transform.SetParent(parent);

            float height = 0.0f;

            if (uiCount > 0)
            {
                RectTransform before = parent.GetChild(uiCount - 1).GetComponent<RectTransform>();
                height = (before.childCount-2) * -35;
                height -= 125 - before.anchoredPosition.y; 
            }

            RectTransform rect = newQuest.GetComponent<RectTransform>();
            rect.localScale = Vector3.one;
            rect.anchoredPosition = new Vector3(0, height, 0.0f);

            GameObject sub = newQuest.transform.GetChild(1).gameObject;
            Text scriptText = sub.GetComponent<Text>();

            if (questList[index].id >= 9500)
                scriptText.text = "???";
            else
                scriptText.text = questList[index].quest[0].QuestScript;

            sub.transform.GetChild(1).GetComponent<Text>().text = "/" + questList[index].quest[0].provisoID.Count.ToString();

            if (questCount >= 2)
            {
                for (int i = 1; i < questCount; i++)
                {
                    GameObject newScript = Instantiate(prefab_script);

                    newScript.transform.SetParent(newQuest.transform);

                    float y =-35 * i;
                    RectTransform scriptRect = newScript.GetComponent<RectTransform>();
                    scriptRect.anchoredPosition = new Vector3(65, y, 0);

                    if (questList[index].id >= 9500)
                        newScript.GetComponent<Text>().text = "???";
                    else
                       newScript.GetComponent<Text>().text = questList[index].quest[i].QuestScript;

                    newScript.transform.GetChild(1).GetComponent<Text>().text = "/" + questList[index].quest[i].provisoID.Count.ToString();
                }
            }
            uiList.Add(newQuest);
           
        }
    }

}

