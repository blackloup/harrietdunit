﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.SceneManagement;
using System.IO;
using System;
using Cinemachine;

// 저장 시스템 타입 
public enum SystemType
{
    New = 0,
    Save,
    Load,
    Delete
}

public enum SceneType
{
    Title=0,
    LibraryIntro,
    Tu_Library,
    P_Room,
    Street,
    Doc_Room,
    Modo_Night,
    DoctorRoom,
    SecretRoom
}

public class GameManager : MonoBehaviour
{

    //싱글톤
    private static GameManager _instance = null;
    public static GameManager Instance { get { return _instance; } }

    [SerializeField]
    private GameObject _main;
    public GameObject Main { get { return _main; } set { _main = value;  } }

    private GameObject _mainVirtual;
    private CinemachineVirtualCamera _virtual;
    public GameObject MainVirtual { get { return _mainVirtual; } set { _mainVirtual = value;} }

    public Vector3 camPos;
    public GameObject canvas;

    GameObject player;
    public GameObject Player { set { player = value; } }
    public void PlayerEnable(bool set) { player.SetActive(set); }

    bool _playEvent = false;
    public bool bPlayEvent {
        get { return _playEvent; }
        set { _playEvent = value; }
    }

    EventManager _event;
    public EventManager Event { get { return _event; } }

    DoctorRoomCtrl _drRoomCtrl;
    public DoctorRoomCtrl DrRoomCtrl { get { return _drRoomCtrl; } }

    //현재 씬
    static SceneType _curScene = SceneType.Title;
    public SceneType CurScene { get { return _curScene; } }

    //다음 씬
    bool _isNext = false;
    public bool IsNext { get { return _isNext; } }

    SceneType _nextScene;
    public void SetNextScene(SceneType type) { _nextScene = type; _isNext = true; }
    public void NextScene()
    {
        _curScene = _nextScene;
        _isNext = false;

        if (_curScene == SceneType.Tu_Library)
        {
            _audio_bgm.enabled = true;
        }

        else
        {
            if (!_audio_bgm.isPlaying)
                _audio_bgm.UnPause();
        }
        
        SceneManager.LoadScene(_curScene.ToString());
    }

    bool _isLoad = false;
    public bool IsLoad { get { return _isLoad; } set { IsLoad = value; } }

    //시작 시간
    DateTime _startTime;
    public DateTime StartTime { get { return _startTime; } }
    public void SetStratTime() { _startTime = DateTime.Now; }

    // 조사 중인지
    bool _isObserve = true;
    public bool IsObserve
    {
        set { _isObserve = value; }
        get { return _isObserve; }
    }

    AudioSource _audio_bgm;
    //public AudioSource Audio;


    private void Awake()
    {
        // 씬에 이미 게임 매니저가 있을 경우
        if (_instance)
        {
            //삭제
            Destroy(gameObject);
            return;
        }
        // 유일한 게임 매니저 
        _instance = this;

        _event = GetComponent<EventManager>();
        _drRoomCtrl = GetComponent<DoctorRoomCtrl>();
        _audio_bgm = GetComponent<AudioSource>();

        canvas.SetActive(false);

        _main = Camera.main.gameObject;
        _main.SetActive(false);

        _mainVirtual = GameObject.Find("V_Start");
        _virtual = _mainVirtual.GetComponent<CinemachineVirtualCamera>();
        _virtual.m_Priority = 1;

        player = GameObject.FindGameObjectWithTag("Player");
        player.SetActive(false);

        DontDestroyOnLoad(gameObject);

    }

    public void Exit()
    {
        Application.Quit();
    }

    //========================================================================
    //                               New Game
    //========================================================================
    public void NewGame()
    {
        TextAsset txt = (TextAsset)Resources.Load("Xml/Note");
        XmlDocument noteDoc = new XmlDocument();
        noteDoc.LoadXml(txt.text);
        noteDoc.Save(Application.dataPath + "/Play_note.xml");

         txt = (TextAsset)Resources.Load("Xml/Observe");
        XmlDocument objDoc = new XmlDocument();
        objDoc.LoadXml(txt.text);
        objDoc.Save(Application.dataPath + "/Play_ob.xml");

        txt = (TextAsset)Resources.Load("Xml/QL");
        XmlDocument QLDoc = new XmlDocument();
        QLDoc.LoadXml(txt.text);
        QLDoc.Save(Application.dataPath + "/Play_QL.xml");

        txt = (TextAsset)Resources.Load("Xml/Select");
        XmlDocument sDoc = new XmlDocument();
        sDoc.LoadXml(txt.text);
         sDoc.Save(Application.dataPath + "/Play_select.xml");

        //게임 시작 
        _curScene = SceneType.Tu_Library;

        SetStratTime();

        canvas.SetActive(true);

        UIManager.Instance.Init();
        _isObserve = false;

        // 카메라 위치변경
        _virtual.m_Priority = 15;

    }

}
