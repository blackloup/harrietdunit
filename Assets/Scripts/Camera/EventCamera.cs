﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class EventCamera : MonoBehaviour
{

    int itemLayer = (1 << 29);
    Camera _camera;
    DetailsCamera _det;

    string qId;
    public string Qid { get { return qId; } }
    public void SetID(string id) { qId = id; }

    bool b_endEvnt = false;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        _det = GetComponent<DetailsCamera>();
    }

    bool init = false;
    private void OnEnable()
    {
        if (!init) { init = true;}
        else
        {
            Invoke("StartEvent", 1.0f);
        }
    }

    void StartEvent()
    {
        b_endEvnt = false;
    }

    private void Update()
    {
        if (!DialoguePanel.Instance.IsTalk && !b_endEvnt && !UIManager.Instance.OpenNote)
        {
           Interaction();

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EndEvent();
            }
        }
    }

    //클릭 시 
    void Interaction()
    {
        Ray r = _camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, 3.0f, itemLayer))
        {
            string name = hit.collider.name;

         //   Vector2 screenPos = _camera.WorldToScreenPoint(Input.mousePosition + new Vector3(80, -10));
        //    ui_f.transform.position = screenPos;

            if (Input.GetMouseButtonDown(0))
            {
                UIManager.Instance.Dialoue.SetActive(true);

                if (hit.collider.tag == "DET")
                {
                    DialoguePanel.Instance.StartDialogue(DialogueType.DET, name);
                    _det.enabled = true;
                    _det.DetailsObserve(name);
                    hit.collider.tag = "EXP";
                }
                else
                    DialoguePanel.Instance.StartDialogue(DialogueType.EXP, name);
            }
        }
    }

    public void EndEvent()
    {
        UIManager.Instance.StartFadeOut();
        b_endEvnt = true;
    }

}