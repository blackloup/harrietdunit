﻿//------------------------------------------------------------------//
// 작성자 : 조성혜
// 미니게임, 확대조사등의 이벤트 매니저 
// 최종 수정일자 : 18.06.17
//------------------------------------------------------------------//
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using UnityEngine.SceneManagement;
using Cinemachine;
using UnityEngine.UI;
using System.Collections;
using System;

struct EventInfo
{
    public string eventName;
    public string cameraName;
    public string unlock;
    public string[] getOnStateID;
}
public class EventManager : MonoBehaviour {

    GameObject _mainCamera;
    GameObject[] _eventCamera;
    List<EventInfo> eventList;

    XmlDocument eventDoc;
    TextAsset eventText;

    XmlDocument objDoc;


	void Awake ()
    {
        eventText = (TextAsset)Resources.Load("Xml/Event");
        
        Init();
    }
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    bool _spwan = false;
    public bool Spwan { set { _spwan = value; } }
    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        SceneType cur = GameManager.Instance.CurScene;
        if (cur != SceneType.Title)
            GameManager.Instance.Main = Camera.main.gameObject;
        if (cur == SceneType.P_Room && _spwan)
        {
            GameObject s = GameObject.Find("State");
            GameObject on = s.transform.GetChild(0).gameObject;
            GameObject off = s.transform.GetChild(1).gameObject;
            on.SetActive(true);
            off.SetActive(false);
        }
        EventCamera[] _event = GameObject.FindObjectsOfType<EventCamera>();

        _eventCamera = new GameObject[_event.Length];
        for (int i = 0; i < _eventCamera.Length; i++)
        {
            _eventCamera[i] = _event[i].gameObject;
            _eventCamera[i].SetActive(false);
        }

     
    }

    private void Init()
    {
        eventList = new List<EventInfo>();
        eventDoc = new XmlDocument();
        eventDoc.LoadXml(eventText.text);

        XmlNode root = eventDoc.SelectSingleNode("Event");
        foreach(XmlNode node in root.ChildNodes)
        {
            EventInfo eInfo = new EventInfo();
            eInfo.eventName = node.Name;
            eInfo.cameraName = node.SelectSingleNode("CameraName").InnerText;

            XmlNode option = node.SelectSingleNode("Option");
            eInfo.unlock = option.Attributes["QuestListUnlock"].Value;

            if (option.Attributes["GetOnStateID"] != null)
            {
                string onId = option.Attributes["GetOnStateID"].Value;
                string[] IDs = onId.Split('/');
                eInfo.getOnStateID = IDs;
            }
            else eInfo.getOnStateID = null;
            eventList.Add(eInfo);
        }

    }


    public void ActiveEvent(string eventName)
    {
     if(eventName.Contains("Scene"))
        {
            string scene = eventName.Replace("Scene_", "");
            GameManager.Instance.SetNextScene((SceneType)Enum.Parse(typeof(SceneType), scene));
            GameManager.Instance.NextScene();
        }
        else {

            UIManager.Instance.StartFadeIn();

            // 같은 이름 찾기
            string camName = null;
            string qId = null;
            foreach (EventInfo e in eventList)
            {
               if(e.cameraName == eventName)
                {
                    camName = e.cameraName;
                    qId = e.unlock;
                    break;
                }
            }

#if UNITY_EDITOR
            if (camName == null)
                Debug.Log(name + ": EventCamera Error");
#endif

            // 퀘스트 언락
             UIManager.Instance.Quest.AddQuest(qId,true);

            UIManager.Instance.StartFadeIn();

            GameManager.Instance.PlayerEnable(false);

            GameManager.Instance.bPlayEvent = true;

            // 카메라 변경
            foreach (GameObject camera in _eventCamera)
            {
                if (camera.name == camName)
                {
                    
                    camera.SetActive(true);
                    camera.GetComponent<EventCamera>().SetID(qId);
                    _mainCamera = GameManager.Instance.Main;
                    _mainCamera.SetActive(false);
                    break;
                }
            }
        }

     

    }

    /*
    IEnumerator FadeIn()
    {
        Color color = panel.color;
        while (color.a > 0)
        {
            color.a -= Time.deltaTime;
            panel.color = color;

            yield return new WaitForSeconds(Time.deltaTime);
        }
        yield break;
    }

    IEnumerator FadeOut()
    {
        Color color = panel.color;
        while (color.a < 1)
        {
            color.a += Time.deltaTime;
            panel.color = color;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        panel.enabled = false;
        _mainCamera.SetActive(true);
        _mainCamera.GetComponent<CameraInteraction>().enabled = true;
        yield break;
    }
*/
}
