﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoctorRoomCtrl : MonoBehaviour
{

    // 닥터 등장 여부 체크 
    bool b_checkDr = false;
    public bool bCheckDr { get { return b_checkDr; } }
    public void setCheck(bool b)
    {
        b_checkDr = b;
    }

    GameObject doctor;
    GameObject secretDoor;
    GameObject openDoor;

    bool _isInit = false;
    public void Init()
    {
        doctor = GameObject.Find("Doctor");
        secretDoor = GameObject.Find("수상한약장");
        openDoor = GameObject.Find("SecretDoor");
        doctor.SetActive(false);
        openDoor.SetActive(false);

        _isInit = true;

    }
    public void CheckDR()
    {
        if (b_checkDr)
        {
            doctor.SetActive(true);
            secretDoor.SetActive(false);
            openDoor.SetActive(true);
        }
    }

    public void OpenLeft()
    {
        Animator ani = secretDoor.transform.GetChild(0).GetComponent<Animator>();
        ani.SetBool("open", true);
    }
}
