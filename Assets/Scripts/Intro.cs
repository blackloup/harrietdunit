﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Intro : MonoBehaviour {

    public GameObject introCam;
    public GameObject titleCam;
    GameObject mainCam;

    public GameObject space;

    public GameObject introObj;

    public PlayableDirector timeLine;

    bool isStart = true;

    private void OnEnable()
    {
        mainCam = GameManager.Instance.Main;
        if (isStart)
        {
            isStart = false;
            StartIntro();

        }
        else
        {
            isStart = true;
            EndIntro();
        }
}

    public void StartIntro()
    {
        introCam.SetActive(true);
        introObj.SetActive(true);
        mainCam.SetActive(false);
        // 시작 버추얼 캠 끄기
        GameManager.Instance.MainVirtual.SetActive(false);

        // 카메라 영역 끄기 
        space.SetActive(false);

        // 타이틀 UI 숨기기
        UIManager.Instance.OffTitle();

        timeLine.Play();
    }

    void EndIntro()
    {

        UIManager.Instance.OnTitle();
        mainCam.SetActive(true);
        introCam.SetActive(false);
        introObj.SetActive(false);

        GameManager.Instance.MainVirtual.SetActive(true);
        space.SetActive(true);

    }

    public void SkipIntro()
    {
        titleCam.SetActive(true);
        timeLine.Pause();
        EndIntro();
    }

}
